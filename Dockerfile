FROM maven:3.9.4-amazoncorretto-11 AS build
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN mvn clean package

FROM scratch
COPY --from=build /app/target/library-0.0.1-SNAPSHOT.war /dist/library.war
